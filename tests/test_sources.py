#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
from io import StringIO
import os
import sys
import unittest

from unittest.mock import patch

import sources

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')


class TestSin(unittest.TestCase):

    def test_simple(self):
        sound = sources.sin(1, 400)
        self.assertEqual(44100, len(sound))
        self.assertEqual(0, sound[0])
        self.assertEqual(1866, sound[1])
        self.assertEqual(17679, sound[10])


if __name__ == '__main__':
    unittest.main(module=__name__, buffer=True, exit=False)
