#!/usr/bin/env python3

"""Record some sound from the default device"""

import sys

import sounddevice
import soundfile

# Souund samples per second
samples_second = 44100

def main():

    if len(sys.argv) != 3:
        print(f"Usage: {sys.argv[0]} <filename> <seconds>")
        exit(0)

    filename = sys.argv[1]
    timespan = int(sys.argv[2])
    samples = timespan * samples_second

    print("Recording... ", end='')
    sound = sounddevice.rec(samples, samplerate=samples_second, channels=2, dtype='int16')
    sounddevice.wait()

    soundfile.write(filename, sound, samples_second)
    print(f"done, saved in {filename}.")


if __name__ == '__main__':
    main()