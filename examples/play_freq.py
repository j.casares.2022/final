#!/usr/bin/env python3

import array
import math
import sys

import sounddevice

# samples per second
samples_second = 44100
# Note duration in seconds
seconds = 1
# Maximum amplitude
max_amp = 32767
# Pi
pi = 3.141592

def main():

    if len(sys.argv) != 2:
        print(f"Usage: {sys.argv[0]} <frequency>")
        exit(0)
    freq = int(sys.argv[1])

    # Total number of samples (samples per second by number of seconds)
    nsamples = samples_second * seconds

    # Create an array of integers (signed shorts, or 16 bit signed integers)
    sound = array.array('h', [0]*nsamples)

    # Compute sin samples (maximum amplitude, freq frequency)
    for nsample in range(nsamples):
        t = nsample / samples_second
        sound[nsample] = int(max_amp * math.sin(2*pi*freq*t))

    # Play audio
    sounddevice.play(sound, samplerate=samples_second, mapping=[1])
    sounddevice.wait()
    sounddevice.play(sound, samplerate=samples_second, mapping=[2])
    sounddevice.wait()
    sounddevice.play(sound, samplerate=samples_second, mapping=[1,2])
    sounddevice.wait()


if __name__ == '__main__':
    main()