"""sinks.py: métodos (funciones) para almacenar o
reproducir de alguna forma el sonido (sumideros de sonido).
Incluye un método play que reproduce el sonido, y un método
draw que escribe en pantalla líneas de asteriscos correspondientes
al valor medio de conjuntos de muestras de la señal. Ambos métodos
aceptarán como primer parámetro el sonido a reproducir o a mostrar.
También hay algunos métodos auxiliares, cuyo nombre comienza por _."""

import config

import sounddevice
import soundfile


def play(sound):
    sounddevice.play(sound, config.samples_second)
    sounddevice.wait()


def _mean(sound):
    total = 0
    for sample in sound:
        total += abs(sample)
    return int(total / len(sound))


def draw(sound, period: float, source):
    samples_period = int(period * config.samples_second)
    for nsample in range(0, len(sound), samples_period):
        chunk = sound[nsample: nsample + samples_period]
        mean_sample = _mean(chunk)
        stars = mean_sample // 1000

        if source == 'square':
            if chunk[0] > 0:
                print('*' * stars)
            else:
                print()
        else:
            print('*' * stars)


"""Método store. Completar el módulo sinks.py con un método 
(función), store que almacene el sonido en un fichero en formato 
WAV. La función aceptará dos parámetros: sound, que será un array 
de muestras de sonido, y path, que será el camino (nombre completo) 
del fichero en que se almacenará el sonido. """

def store(sound, path: str):
    soundfile.write(path, sound, config.samples_second)#guardar(WAV)
