"""sources.py: métodos (funciones) para producir sonido digital
de distintas formas (fuentes de sonido). Incluye un método sin
que produce una señal sinusoidal de una frecuencia dada, y un
método constant que produce una señal constante. Ambas funciones
devuelven el sonido como un vector (array) de muestras, donde cada
muestra es un número entero de dos bytes. Ambas funciones aceptan
como primer parámetro la duración del sonido a producir.
También hay algunos métodos auxiliares, cuyo nombre comienza por _."""

import array
import math
import config


def _samples(duration):
    """Compute the number of samples for a certain duration (sec)"""

    # Total number of samples (samples per second by number of seconds)
    return int(config.samples_second * duration)


def sin(duration: float, freq: float):
    """Produce a list of samples of a sinusoidal signal, of duratioon (sec)"""

    nsamples = _samples(duration)

    # Create an array of integers (signed shorts: 16 bit signed integers)
    sound = array.array('h', [0] * nsamples)

    # Compute sin samples (maximum amplitude, freq frequency)
    for nsample in range(nsamples):
        t = nsample / config.samples_second
        sound[nsample] = int(config.max_amp * math.sin(2 * config.pi * freq * t))

    return sound


def constant(duration: float, positive: bool):
    """Produce a list of samples of a constant signal, of duration (sec)"""

    nsamples = _samples(duration)

    # Create an array of integers with either max or min amplitude
    if positive:
        sample = config.max_amp
    else:
        sample = - config.max_amp

    sound = array.array('h', [sample] * nsamples)

    return sound


"""Método square. Completar el módulo sources.py con un método 
(función), square, que produzca una señal cuadrada: durante un 
tiempo estará a la máxima amplitud, a continuación, durante el 
mismo tiempo, a la mínima amplitud (negativa), luego de nuevo a 
la máxima, y así sucesivamente. La función aceptará dos 
parámetros: duration, que será la duración, en segundos, del 
sonido producido, y freq, que será la frecuencia de la señal 
(cuántas veces por segundo la señal estará en el máximo)."""
def square(duration: float, freq: float):
    """Produce a list of samples of a square signal, of duration(sec)."""

    nsamples = _samples(duration)

    # Create an array of integers (signed shorts: 16 bit signed integers)
    sound = array.array('h', [0] * nsamples)
    x = 1

    # Compute square samples (maximum amplitude, freq frequency)
    for nsample in range(nsamples):
        if nsample % freq == 0:
            x *= -1
        sound[nsample] = (config.max_amp * x)


    return sound
