"""Se creará un nuevo módulo que pueda funcionar como progrma
principal, con la misma estructura que source_sink.py (una función
main que tendrá el programa principal, y que se ejecutará cuando el
módulo sea invocado como programa principal). Este programa
ejecutará primero una fuente, después un procesador, y por último
un sumidero de sonido. Admitirá como argumentos un nombre de fuente,
uno de procesador, uno de sumidero, y una duración. Los nombres
de fuente, procesador y sumidero serán los nombres de las funciones
correspondientes en sources.py, processes.py y sinks.py."""


import sys
import processes
import sources
import sinks


def parse_args():
    if len(sys.argv) != 5:
        print(f"Usage: {sys.argv[0]} <source> <processes> <sink> <duration>")
        exit(0)
    source = sys.argv[1]
    process = sys.argv[2]
    sink = sys.argv[3]
    duration = float(sys.argv[4])
    return source, process, sink, duration


def main():
    source, process, sink, dur = parse_args()

    if source == 'sin':
        sound = sources.sin(duration=dur, freq=440)
    elif source == 'constant':
        sound = sources.constant(duration=dur, positive=True)
    elif source == 'square':
        sound = sources.square(duration=dur, freq=440)
    else:
        sys.exit("Unknown source")

    if process == 'ampli':
        processes.ampli(sound=sound, factor=20)
    elif process == 'reduce':
        processes.reduce(sound=sound, factor=20)
    elif process == 'extend':
        processes.extend(sound=sound, factor=20)
    else:
        sys.exit("Unknown porcess")

    if sink == "play":
        sinks.play(sound)
    if sink == "draw":
        sinks.draw(sound=sound, period=0.0001, source=source)
    if sink == 'store':
        sinks.store(sound=sound, path='stored.wav')
    else:
        sys.exit("Unknown sink")


if __name__ == '__main__':
    main()