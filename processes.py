"""Método ampli. Crear un nuevo módulo, processes.py
que incluirá un método (función), ampli que amplificará el sonido
multiplicando el valor de cada muestra por un número dado. Si el
resultado de la multiplicación es mayor que la amplitud máxima,
o menor que la amplitud mínima, el valor de la muestra amplificada
será ese máximo o mínimo, respectivamente. La función aceptará dos
parámetros: sound (el sonido a amplificar) y factor (el factor de
amplificación, que será un número float, y que se usará para
multimplicar cada muestra por él)."""


import array
import sys
import config


def ampli(sound, factor: float):
    nsamples = config.samples_second * int(sys.argv[4])
    sounds = array.array('h', [0] * nsamples)
    for i in range(nsamples):
        if sounds[i] > config.max_amp:
            sounds[i] = config.max_amp
        elif sounds[i] < -config.min_amp:
            sounds[i] = -config.min_amp
        else:
            sounds[i] *= factor
    return sound

"""Método reduce. Crear en el módulo processes.py un método 
(función), reduce que reducirá el número de muestras de sonido 
quitando una de cada tantas que se le indica. Por ejemplo, si se 
indica "3", quitará una de cada tres: la tercera, la sexta, la 
novena, y así sucesivamente. En este caso, por "quitar" se 
entiende que la muestra no estará en el sonido que devuelva la 
función. Naturalmente, la duración del sonido quedará reducida en 
la misma proporción (si un sonido de tres segundos se reduce "uno 
de cada tres", quedará en dos segundos). La función aceptará dos 
parámetros: sound (el sonido a reducir) y factor (el factor de 
reducción, que será un número int mayor que 0)."""

def reduce(sound, factor: int):
    nsamples = config.samples_second * int(sys.argv[4])
    sounds = array.array('h', [0] * nsamples)

    if factor > 0:
        del sounds[::factor]
        return sound
    else:
        print("Tiene que ser estrictamente mayor que 0.")


"""Método extend. Crear en el módulo processes.py un método 
(función), extend que extenderá el número de muestras de sonido 
añadiendo una más cada tantas que se le indica. Por ejemplo, si se 
indica "3", añadirá una cada tres: tras las tres primeras una 
cuarta, tras las tres siguientes una octava, y así sucesivamente. 
En este caso, por "añadir" se entiende que la nueva muestra se 
colocará después de las indicadas, y se calculará como la media 
entre la última de esas muestras, y la muestra siguiente. 
Naturalmente, la duración del sonido quedará extendida en la 
misma proporción (si un sonido de tres segundos se extiende "uno 
de cada tres", quedará en cuatro segundos). La función aceptará 
dos parámetros: sound (el sonido a reducir) y factor (el factor 
de extensión, que será un número int mayor que 0)."""

def extend(sound, factor: int):
    nsamples = config.samples_second * int(sys.argv[4])
    x = 0

    if factor > 0:
        for i in range(factor, nsamples, factor):
            if i > factor:
                i *= x
                sound.insert(i, (sound[i - 1]) + sound[1] // 2)
            else:
                sound.insert(i, (sound[i - 1]) + sound[1] // 2)
        return sound
    else:
        print("Error, tiene que ser estrictamente mayor que 0.")